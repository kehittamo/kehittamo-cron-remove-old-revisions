<?php
/*
Plugin Name: Kehittämö cronjob remove old revisions
Plugin URI: http://www.kehittamo.fi
Description: Cronjob run monthly and find from all posts (modified_gmt) which are older than 1 year and remove all revisions
Version: 0.1.0
Author: Kehittämö Oy / Jani Krunniniva
Author Email: asiakaspalvelu@kehittamo.fi
License: GPL2

  Copyright 2016 Kehittämö Oy (asiakaspalvelu@kehittamo.fi)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/
namespace Kehittamo\Plugins\CronjobRemoveRevisions;

define( 'Kehittamo\Plugins\CronjobRemoveRevisions\PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'Kehittamo\Plugins\CronjobRemoveRevisions\PLUGIN_URL', plugin_dir_url( __FILE__ ) );

/**
 * NOTES:
 * https://wordpress.org/plugins/core-control/ is good plugin for check what cronjobs is currently running
 */
class Load {
  /**
   * Construct the plugin
   */

  const cron_remove_revisions = 'kehittamo_cron_remove_revisions';    // cronjob name
  const MAX_REVISIONS = 20;  // max revisions to posts

  function __construct() {
    add_filter( 'wp_revisions_to_keep', array( $this, 'set_max_revisions_to_wp' ), 10, 2 );	// set max revisions
    add_filter( 'cron_schedules', array( $this, 'add_new_intervals' ) );  // add new intervals for cronjobs
    add_action( 'init', array( $this, 'add_new_schedules' ) );  //add new schedules

    // actions for schedule events
    //add_action( cron_remove_revisions, array( $this, 'test_post' ) );
    add_action( self::cron_remove_revisions, array( $this, 'find_revisions' ) );
  }

  /**
   * Set max revisions to posts
   */
  function set_max_revisions_to_wp( $num, $post ) {
    $num = self::MAX_REVISIONS;

    return $num;
  }

  /**
   * Clear specific cronjob
   * NOTE not used in the code yet. Just if needed to clear hook
   */
  function clear_cron_jobs() {
    wp_clear_scheduled_hook( self::cron_remove_revisions );
  }

  /**
   * Add new schedules
   */
  function add_new_schedules() {
    // check is cronjob already running

    if ( ! wp_next_scheduled( self::cron_remove_revisions ) ) {
      wp_schedule_event( time(), 'monthly', self::cron_remove_revisions );
    }

  }

  /**
   * Add custom cronjob intervals
   */
  function add_new_intervals( $schedules ) {
    if ( 'development' === getenv( 'WP_ENVIRONMENT' ) ) {
      $schedules['5seconds'] = array(         // Provide the programmatic name to be used in code
        'interval' => 5,                      // Intervals are listed in seconds
        'display'  => __( 'Every 5 Seconds' ) // Easy to read display name
      );
    }

    $schedules['monthly'] = array(
      'interval' => 2635200,
      'display'  => __( 'Once a month' )
    );

    return $schedules;
  }

  /**
   * Find revisions
   */
  function find_revisions() {
    // get all posts which are modified more than 1 years ago
    $args  = array(
      'posts_per_page' => - 1,
      'post_type'      => 'post',
      'date_query'     => array(
        'column' => 'post_date_gmt', // post_modified_gmt // post_date_gmt is for if needed to test,
        'before' => date( 'Y-m-d', strtotime( '-1 year' ) )
      ),
      'cache_results'  => false,
      'no_found_rows'  => true
    );
    $posts = get_posts( $args );

    // go thru with posts
    foreach ( $posts as $post ) {
      $revisions = wp_get_post_revisions( $post->ID );

      // check is post, has revisions, is older then 1 year, check is post revision
      if ( isset( $post ) && is_array( $revisions ) && count( $revisions ) >= 1 && ! wp_is_post_revision( $post->ID ) ) {
        $this->remove_all_revisions_from_post( $post->ID );  // remove revisions
      }
    }
  }

  /**
   * Remove revisions from post
   */
  function remove_all_revisions_from_post( $post_id ) {
    $revisions = wp_get_post_revisions( $post_id );  // get revisions

    foreach ( $revisions as $revision ) {
      if ( wp_is_post_revision( $revision ) ) {
        // remove current revision
        wp_delete_post_revision( $revision->ID );
        //$this->test_post('Revision removed:' . $revision->ID . ' : '); // for the debugging
      }
    }
  }

  /**
   * TEST CASE FOR SCHEDULED ACTION
   * create test post
   */
  function test_post( $title = 'My post ', $content = 'This is my post.' ) {
    $my_post = array(
      'post_title'    => $title . date( 'Y-m-d H:i:s', current_time( 'timestamp', 0 ) ),
      'post_content'  => $content,
      'post_status'   => 'publish',
      'post_author'   => 1,
      'post_category' => array( 8, 39 )
    );
    // Insert the post into the database.
    wp_insert_post( $my_post );
  }
}

$kehittamo_cronjob_remove_revisions = new \Kehittamo\Plugins\CronjobRemoveRevisions\Load();

